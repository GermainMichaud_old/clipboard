import React, { useCallback, useEffect, useState } from 'react';
import './App.css';
import { formatDistanceToNow } from "date-fns";
import { fr } from "date-fns/locale";
// Prod
const { clipboard } = window.require('electron')

// Dev
// class Clipboard {
//   state = {
//     text: 'test'
//   }
  
//   readText() {
//     return this.state.text
//   }
//   writeText(item) {
//     this.state.text = item
//   }
// }

// const clipboard = new Clipboard()

function App() {
  
  const [history, setHistory] = useState([{text: clipboard.readText(), clipped: new Date()}])
  const audio = new Audio('./clipboard.mp3')

  const checkClipboard = async () => {
    try {
      const text = await clipboard.readText()
      if (history.length && history[history.length - 1].text !== text) {
        setHistory(prev => {
          return [...prev, { text, clipped: new Date()}]
        })
        audio.currentTime = 0
        audio.play()
      }
    } catch (e) {
      console.log('Error copy => ', e)
    }
  }

  const itemClicked = useCallback((item) => {
    const index = history.indexOf(item)
    if (index < history.length - 1 && history.length > 1) {
      setHistory(previous => {
        let data = previous
        let spliced = data.splice(index, 1)
        return [...data, spliced[0]]
      })
    }
    clipboard.writeText(item.text)
    window.scrollTo(0,0)
  }, [history])

  useEffect(() => {
    const check = setInterval(() => {
      checkClipboard()
    }, 500)

    return () => {
      clearInterval(check)
    }
  }, [setHistory, checkClipboard])

  return (
    <div className="App">
      <h1>Clipboard</h1>
      <div className="liste">
      {history.slice().reverse().map((el, idx) => (
        <div className="item" key={idx} onClick={() => itemClicked(el)}>
          <pre>{el.text}</pre>
          <small>{formatDistanceToNow(new Date(el.clipped), {locale:fr, addSuffix: true})}</small>
        </div>
      ))}
        </div>
    </div>
  );
}

export default App;
